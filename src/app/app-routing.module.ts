import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {TableProductsComponent} from './components/products/table-products/table-products.component';
import {LoginComponent} from './components/login/login.component';
import {AuthGuard} from './guards/auth.guard';
import {TableReceptionComponent} from './components/receptions/table-reception/table-reception.component';
import {TableDeliveriesComponent} from './components/deliveries/table-deliveries/table-deliveries.component';
import {TablePersonalComponent} from './components/personaal/table-personal/table-personal.component';
import {TablesTypesComponent} from './components/types/tables-types/tables-types.component';
import {TableUsersComponent} from './components/users/table-users/table-users.component';
import {TableInventoryComponent} from './components/inventory/table-inventory/table-inventory.component';
import {ModalDetailComponent} from './components/inventory/modal-detail/modal-detail.component';
import {DetailInventoryComponent} from './components/inventory/detail-inventory/detail-inventory.component';
import {LoginResetPassComponent} from './components/login-reset-pass/login-reset-pass.component';
import {LoginGuard} from './guards/login.guard';

const routes: Routes = [
  {path: '', component: TableProductsComponent, canActivate: [AuthGuard]},
  {path: 'login', component: LoginComponent},
  {path: 'reLogin', component: LoginResetPassComponent, canActivate: [LoginGuard]},
  {
    path: 'root', children: [
      {path: 'inventories', component: TableInventoryComponent, canActivate: [AuthGuard]},
      {path: 'users', component: TableUsersComponent, canActivate: [AuthGuard]},
    ]
  },
  {
    path: 'admin/:id', children: [
      {path: 'products', component: TableProductsComponent, canActivate: [AuthGuard]},
      {path: 'receptions', component: TableReceptionComponent, canActivate: [AuthGuard]},
      {path: 'deliveries', component: TableDeliveriesComponent, canActivate: [AuthGuard]},
      {path: 'personal', component: TablePersonalComponent, canActivate: [AuthGuard]},
      {path: 'unities', component: TablesTypesComponent, canActivate: [AuthGuard]},
      {path: 'users', component: TableUsersComponent, canActivate: [AuthGuard]},
    ]
  },
  {
    path: 'supervisor', children: [
      {path: 'department/:id', component: ModalDetailComponent},
      {path: 'inventory/:id', component: DetailInventoryComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
