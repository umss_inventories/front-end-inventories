import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MaterialModule} from './modules/material.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {TableProductsComponent} from './components/products/table-products/table-products.component';
import {PanelNavigationComponent} from './components/panel-navigation/panel-navigation.component';
import {TableToolbarComponent} from './components/table-toolbar/table-toolbar.component';
import {PanelRightComponent} from './components/panel-right/panel-right.component';
import {SideNavService} from './services/side-nav.service';
import {LoginComponent} from './components/login/login.component';
import {ProductsService} from './services/products.service';
import {AuthInterceptor} from './services/auth.interceptor';
import {FormProductComponent} from './components/products/form-product/form-product.component';
import {UnityService} from './services/unity.service';
import {SnackSuccessComponent} from './components/dialogs/snack-success/snack-success.component';
import {SnackErrorComponent} from './components/dialogs/snack-error/snack-error.component';
import {SnackAlertComponent} from './components/dialogs/snack-alert/snack-alert.component';
import {DeleteDialogComponent} from './components/dialogs/delete-dialog/delete-dialog.component';
import {FormReceptionComponent} from './components/receptions/form-reception/form-reception.component';
import {TableReceptionComponent} from './components/receptions/table-reception/table-reception.component';
import {FormDeliveryComponent} from './components/deliveries/form-delivery/form-delivery.component';
import {TableDeliveriesComponent} from './components/deliveries/table-deliveries/table-deliveries.component';
import {ReceptionService} from './services/reception.service';
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {DeliveryService} from './services/delivery.service';
import {PersonalService} from './services/personal.service';
import {FormPersonalComponent} from './components/personaal/form-personal/form-personal.component';
import {TablePersonalComponent} from './components/personaal/table-personal/table-personal.component';
import {MatPaginatorIntl} from '@angular/material';
import {getDutchPaginatorIntl} from './services/Tools';
import {FormTypesComponent} from './components/types/form-types/form-types.component';
import {TablesTypesComponent} from './components/types/tables-types/tables-types.component';
import {SnackDeleteComponent} from './components/dialogs/snack-delete/snack-delete.component';
import {DatePipePipe} from './pipes/date-pipe.pipe';
import {FormUserComponent} from './components/users/form-user/form-user.component';
import {TableUsersComponent} from './components/users/table-users/table-users.component';
import {UserService} from './services/user.service';
import {InventoryService} from './services/inventory.service';
import {InventoryUserService} from './services/inventory-user.service';
import {TableInventoryComponent} from './components/inventory/table-inventory/table-inventory.component';
import {FormInventoryComponent} from './components/inventory/form-inventory/form-inventory.component';
import {RolesService} from './services/roles.service';
import {FormUserRoleComponent} from './components/users/form-user-role/form-user-role.component';
import {TableAdminUsersComponent} from './components/users/table-admin-users/table-admin-users.component';
import {AddAdminInventoryComponent} from './components/inventory/add-admin-inventory/add-admin-inventory.component';
import {PrincipalService} from './services/principal.service';
import {PdfService} from './services/pdf.service';
import {SearchPipe} from './pipes/Search.pipe';
import {FilterDateComponent} from './components/deliveries/filter-date/filter-date.component';
import {ModalDetailComponent} from './components/inventory/modal-detail/modal-detail.component';
import {DetailInventoryComponent} from './components/inventory/detail-inventory/detail-inventory.component';
import {DialogReportComponent} from './components/inventory/dialog-report/dialog-report.component';
import {SetPassComponent} from './components/users/set-pass/set-pass.component';
import {ResetUserPassComponent} from './components/users/reset-user-pass/reset-user-pass.component';
import {LoginResetPassComponent} from './components/login-reset-pass/login-reset-pass.component';

@NgModule({
  declarations: [
    AppComponent,
    TableProductsComponent,
    PanelNavigationComponent,
    TableToolbarComponent,
    PanelRightComponent,
    LoginComponent,
    FormProductComponent,
    SnackSuccessComponent,
    SnackErrorComponent,
    SnackAlertComponent,
    DeleteDialogComponent,
    FormReceptionComponent,
    TableReceptionComponent,
    FormDeliveryComponent,
    TableDeliveriesComponent,
    FormPersonalComponent,
    TablePersonalComponent,
    FormTypesComponent,
    TablesTypesComponent,
    SnackDeleteComponent,
    DatePipePipe,
    FormUserComponent,
    TableUsersComponent,
    TableInventoryComponent,
    FormInventoryComponent,
    FormUserRoleComponent,
    TableAdminUsersComponent,
    AddAdminInventoryComponent,
    SearchPipe,
    FilterDateComponent,
    ModalDetailComponent,
    DetailInventoryComponent,
    DialogReportComponent,
    SetPassComponent,
    ResetUserPassComponent,
    LoginResetPassComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    },
    {provide: MAT_DATE_LOCALE, useValue: 'es_ES'},
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS},
    {provide: MatPaginatorIntl, useValue: getDutchPaginatorIntl()},
    PrincipalService,
    SideNavService,
    ProductsService,
    ReceptionService,
    DeliveryService,
    PersonalService,
    UnityService,
    UserService,
    InventoryService,
    InventoryUserService,
    RolesService,
    PdfService,
  ],
  entryComponents: [
    SnackAlertComponent,
    SnackErrorComponent,
    SnackSuccessComponent,
    SnackDeleteComponent,
    DeleteDialogComponent,
    FormProductComponent,
    FormReceptionComponent,
    FormDeliveryComponent,
    FormPersonalComponent,
    FormTypesComponent,
    FormUserComponent,
    FormUserRoleComponent,
    FormInventoryComponent,
    FilterDateComponent,
    ModalDetailComponent,
    DialogReportComponent,
    SetPassComponent,
    ResetUserPassComponent,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
