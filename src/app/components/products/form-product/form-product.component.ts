import {Component, EventEmitter, Inject, OnInit, Output} from '@angular/core';
import {Unity} from '../../../model/unity';
import {UnityService} from '../../../services/unity.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Product} from '../../../model/products';

@Component({
  selector: 'app-form-product',
  templateUrl: './form-product.component.html',
  styleUrls: ['./form-product.component.scss']
})
export class FormProductComponent implements OnInit {
  unities: Unity[] = [];
  form: FormGroup;
  constructor(private unityService: UnityService,
              private fb: FormBuilder,
              public dialogRef: MatDialogRef<FormProductComponent>,
              @Inject(MAT_DIALOG_DATA) public data: Product) {
    this.form = this.fb.group({
        id: this.data.id,
        name: [this.data.name, [Validators.required]],
        id_unity: [this.data.id_unity, Validators.required],
      }
    );
  }

  ngOnInit() {
    this.unityService.getUnities().subscribe(res => {
      this.unities = res['unities'];
    });
  }
  create() {
    this.dialogRef.close(this.form.value);
  }
  close() {
    this.dialogRef.close(null);
  }

}
