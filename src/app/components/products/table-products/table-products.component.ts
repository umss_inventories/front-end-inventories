import {Component, OnInit, ViewChild} from '@angular/core';
import {ProductsService} from '../../../services/products.service';
import {Product} from '../../../model/products';
import {SideNavService} from '../../../services/side-nav.service';
import {Crud} from '../../crud';
import {MatDialog, MatPaginator, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';
import {SnackSuccessComponent} from '../../dialogs/snack-success/snack-success.component';
import {SnackErrorComponent} from '../../dialogs/snack-error/snack-error.component';
import {FormProductComponent} from '../form-product/form-product.component';
import {DeleteDialogComponent} from '../../dialogs/delete-dialog/delete-dialog.component';
import {SnackDeleteComponent} from '../../dialogs/snack-delete/snack-delete.component';
import {PdfService} from '../../../services/pdf.service';


@Component({
  selector: 'app-table-products',
  templateUrl: './table-products.component.html',
  styleUrls: ['./table-products.component.scss']
})
export class TableProductsComponent implements Crud, OnInit {
  displayedColumns: string[] = ['position', 'name', 'unity', 'quantity', 'option'];
  title = 'Productos';
  dataSource = new MatTableDataSource<Product>([]);
  loader = false;
  productSelect: Product;
  private pageIndex = 0;
  private pageSize = 10;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  constructor(private pService: ProductsService,
              private sideService:  SideNavService,
              private snack: MatSnackBar,
              public dialog: MatDialog,
              private pdf: PdfService) {
  }
  ngOnInit() {
    this.loader = true;
    this.pService.getIndex().subscribe(res => {
      this.dataSource = new MatTableDataSource<Product>(res['products']);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.loader = false;
    });
  }
  newProduct() {
    const dialogRef = this.dialog.open(FormProductComponent, {
      width: '300px',
      data: new Product()
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      if (result) {
        this.productSelect = result;
        this.store();
      }
    });
  }
  count(pos) {
    return pos + (this.pageSize * this.pageIndex) + 1;
  }
  setProduct(p: Product) {
    this.productSelect = p;
  }
  edit() {
    const dialogRef = this.dialog.open(FormProductComponent, {
      width: '300px',
      data: this.productSelect
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.productSelect = result;
        this.store();
      }
    });
  }
  store() {
    this.loader = true;
    if (!this.productSelect.id) {
      this.pService.create(this.productSelect).subscribe(res => {
        this.ngOnInit();
        this.snack.openFromComponent(SnackSuccessComponent, {duration: 2000});
      }, error1 => {
        this.loader = false;
        this.snack.openFromComponent(SnackErrorComponent, {duration: 2000});
      });
    } else {
      this.pService.update(this.productSelect, this.productSelect.id).subscribe(res => {
        this.ngOnInit();
        this.snack.openFromComponent(SnackSuccessComponent, {duration: 2000});
      }, error1 => {
        this.loader = false;
        this.snack.openFromComponent(SnackErrorComponent, {duration: 2000});
      });
    }
  }
  delete() {
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '300px',
      data: this.productSelect.name
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.pService.destroy(this.productSelect.id).subscribe(res => {
          this.ngOnInit();
          this.snack.openFromComponent(SnackDeleteComponent, {duration: 2000});
        }, error1 => {
          this.loader = false;
          this.snack.openFromComponent(SnackErrorComponent, {duration: 2000});
        });
      }
    });
  }

  printF() {
    this.print(this.dataSource.data);
  }


  print(data) {
    const head = ['NRO', 'PRODUCTO', 'CANTIDAD', 'UNIDAD'];
    const body = this.getData(data);
    this.pdf.createSingleTable('REPORTE DE PRODUCTOS', head, body, null,
      {
        0: {
          halign: 'left',
          // columnWidth: 5
        },
        1: {
          // columnWidth: 10
        },
        2: {
          // columnWidth: 20
        },
        3: {
          halign: 'right',
          columnWidth: 10
        },
      });
    this.pdf.printPdf();
  }

  getData(arrayData: Product[]) {
    const data = [];
    arrayData.forEach((c, index) => {
      const d = [];
      d.push(index + 1);
      d.push(c.name.toUpperCase());
      d.push(c.quantity);
      d.push(c.unity.toUpperCase());
      data.push(d);
    });
    return data;
  }

  sety(t) {
    this.pageIndex = t.pageIndex;
    this.pageSize = t.pageSize;
  }

  filter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
