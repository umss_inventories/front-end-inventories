import {Component, OnInit, ViewChild} from '@angular/core';
import {Crud} from '../../crud';
import {MatDialog, MatPaginator, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';
import {SideNavService} from '../../../services/side-nav.service';
import {SnackSuccessComponent} from '../../dialogs/snack-success/snack-success.component';
import {SnackErrorComponent} from '../../dialogs/snack-error/snack-error.component';
import {DeleteDialogComponent} from '../../dialogs/delete-dialog/delete-dialog.component';
import {Personal} from '../../../model/personal';
import {PersonalService} from '../../../services/personal.service';
import {FormPersonalComponent} from '../form-personal/form-personal.component';
import {SnackDeleteComponent} from '../../dialogs/snack-delete/snack-delete.component';

@Component({
  selector: 'app-table-personal',
  templateUrl: './table-personal.component.html',
  styleUrls: ['./table-personal.component.scss']
})
export class TablePersonalComponent implements Crud, OnInit {
  displayedColumns: string[] = ['position', 'name', 'option'];
  title = 'Personal';
  dataSource = new MatTableDataSource<Personal>([]);
  loader = false;
  personalSelect: Personal;
  private pageIndex = 0;
  private pageSize = 10;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  constructor(private personalService: PersonalService,
              private sideService: SideNavService,
              private snack: MatSnackBar,
              public dialog: MatDialog) {
  }

  ngOnInit() {
    this.loader = true;
    this.personalService.getIndex().subscribe(res => {
      this.dataSource = new MatTableDataSource<Personal>(res['personals']);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.loader = false;
    });
  }

  newProduct() {
    const dialogRef = this.dialog.open(FormPersonalComponent, {
      width: '300px',
      data: new Personal()
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.personalSelect = result;
        this.store();
      }
    });
  }

  count(pos) {
    return pos + (this.pageSize * this.pageIndex) + 1;
  }

  setProduct(p: Personal) {
    this.personalSelect = p;
  }

  edit() {
    const dialogRef = this.dialog.open(FormPersonalComponent, {
      width: '300px',
      data: this.personalSelect
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.personalSelect = result;
        this.store();
      }
    });
  }

  store() {
    this.loader = true;
    if (!this.personalSelect.id) {
      this.personalService.create(this.personalSelect).subscribe(res => {
        this.ngOnInit();
        this.snack.openFromComponent(SnackSuccessComponent, {duration: 2000});
      }, error1 => {
        this.loader = false;
        this.snack.openFromComponent(SnackErrorComponent, {duration: 2000});
      });
    } else {
      this.personalService.update(this.personalSelect, this.personalSelect.id).subscribe(res => {
        this.ngOnInit();
        this.snack.openFromComponent(SnackSuccessComponent, {duration: 2000});
      }, error1 => {
        this.loader = false;
        this.snack.openFromComponent(SnackErrorComponent, {duration: 2000});
      });
    }
  }

  delete() {
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '300px',
      data: this.personalSelect.name
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.personalService.destroy(this.personalSelect.id).subscribe(res => {
          this.ngOnInit();
          this.snack.openFromComponent(SnackDeleteComponent, {duration: 2000});
        }, error1 => {
          this.loader = false;
          this.snack.openFromComponent(SnackErrorComponent, {duration: 2000});
        });
      }
    });
  }

  sety(t) {
    this.pageIndex = t.pageIndex;
    this.pageSize = t.pageSize;
  }

  filter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
