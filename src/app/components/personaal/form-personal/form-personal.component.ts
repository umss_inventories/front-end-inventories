import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UnityService} from '../../../services/unity.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Personal} from '../../../model/personal';

@Component({
  selector: 'app-form-personal',
  templateUrl: './form-personal.component.html',
  styleUrls: ['./form-personal.component.scss']
})
export class FormPersonalComponent implements OnInit {
  form: FormGroup;
  constructor(private unityService: UnityService,
              private fb: FormBuilder,
              public dialogRef: MatDialogRef<FormPersonalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: Personal) {
    this.form = this.fb.group({
        id: this.data.id,
        name: [this.data.name, [Validators.required]],
      }
    );
  }

  ngOnInit() {
  }
  create() {
    this.dialogRef.close(this.form.value);
  }
  close() {
    this.dialogRef.close(null);
  }

}
