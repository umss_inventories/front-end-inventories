import {Component, OnInit, ViewChild} from '@angular/core';
import {Crud} from '../../crud';
import {MatDialog, MatPaginator, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';
import {SideNavService} from '../../../services/side-nav.service';
import {SnackSuccessComponent} from '../../dialogs/snack-success/snack-success.component';
import {SnackErrorComponent} from '../../dialogs/snack-error/snack-error.component';
import {DeleteDialogComponent} from '../../dialogs/delete-dialog/delete-dialog.component';
import {DeliveryService} from '../../../services/delivery.service';
import {Delivery} from '../../../model/delivery';
import {FormDeliveryComponent} from '../form-delivery/form-delivery.component';
import {SnackDeleteComponent} from '../../dialogs/snack-delete/snack-delete.component';
import {PdfService} from '../../../services/pdf.service';
import {FilterDateComponent} from '../filter-date/filter-date.component';
import * as moment from 'moment';

@Component({
  selector: 'app-table-deliveries',
  templateUrl: './table-deliveries.component.html',
  styleUrls: ['./table-deliveries.component.scss']
})
export class TableDeliveriesComponent implements Crud, OnInit {
  displayedColumns: string[] = ['position', 'product', 'unity', 'quantity', 'user_name', 'user_personal', 'date', 'option'];
  title = 'Despachos';
  dataSource = new MatTableDataSource<Delivery>([]);
  loader = false;
  deliverySelect: Delivery;
  private pageIndex = 0;
  private pageSize = 10;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  constructor(private deliveryService: DeliveryService,
              private sideService:  SideNavService,
              private snack: MatSnackBar,
              public dialog: MatDialog,
              private pdf: PdfService) {
  }
  ngOnInit() {
    this.loader = true;
    this.deliveryService.getIndex().subscribe(res => {
      this.dataSource = new MatTableDataSource<Delivery>(res['deliveries']);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.loader = false;
    });
  }
  newReception() {
    const dialogRef = this.dialog.open(FormDeliveryComponent, {
      width: '300px',
      data: new Delivery()
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.deliverySelect = result;
        this.store();
      }
    });
  }
  count(pos) {
    return pos + (this.pageSize * this.pageIndex) + 1;
  }
  setProduct(p: Delivery) {
    this.deliverySelect = p;
  }
  edit() {
    const dialogRef = this.dialog.open(FormDeliveryComponent, {
      width: '300px',
      data: this.deliverySelect
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.deliverySelect = result;
        this.store();
      }
    });
  }
  store() {
    this.loader = true;
    if (!this.deliverySelect.id) {
      this.deliveryService.create(this.deliverySelect).subscribe(res => {
        this.ngOnInit();
        this.snack.openFromComponent(SnackSuccessComponent, {duration: 2000});
      }, error1 => {
        this.loader = false;
        this.snack.openFromComponent(SnackErrorComponent, {duration: 2000});
      });
    } else {
      this.deliveryService.update(this.deliverySelect, this.deliverySelect.id).subscribe(res => {
        this.ngOnInit();
        this.snack.openFromComponent(SnackSuccessComponent, {duration: 2000});
      }, error1 => {
        this.loader = false;
        this.snack.openFromComponent(SnackErrorComponent, {duration: 2000});
      });
    }
  }
  delete() {
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '300px',
      data: this.deliverySelect.product
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.deliveryService.destroy(this.deliverySelect.id).subscribe(res => {
          this.ngOnInit();
          this.snack.openFromComponent(SnackDeleteComponent, {duration: 2000});
        }, error1 => {
          this.loader = false;
          this.snack.openFromComponent(SnackErrorComponent, {duration: 2000});
        });
      }
    });
  }

  sety(t) {
    this.pageIndex = t.pageIndex;
    this.pageSize = t.pageSize;
  }

  filterDate() {
    const dialogRef = this.dialog.open(FilterDateComponent, {
      width: '300px',
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.print(this.filterOnDate(result.dateStar, result.dateEnd));
      }
    });
  }

  print(data) {
    const head = ['NRO', 'PRODUCTO', 'FUNCIONARIO', 'ENTREGADO', 'UNIDAD', 'FECHA', 'FIRMA'];
    const body = this.getData(data);
    this.pdf.createSingleTable('REPORTE DE ENTREGAS', head, body, null,
      {
        0: {
          halign: 'left',
          // columnWidth: 5
        },
        1: {
          // columnWidth: 10
        },
        2: {
          // columnWidth: 20
        },
        3: {
          halign: 'right',
          columnWidth: 10
        },
        6: {
          halign: 'right',
          // columnWidth: 20
        },
      });
    this.pdf.printPdf();
  }

  getData(arrayData: Delivery[]) {
    const data = [];
    arrayData.forEach((c, index) => {
      const d = [];
      d.push(index + 1);
      d.push(c.product.toUpperCase());
      d.push(c.user_personal.toUpperCase());
      d.push(Math.abs(c.quantity));
      d.push(c.unity);
      d.push(c.date);
      d.push('');
      data.push(d);
    });
    return data;
  }

  filter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  filterOnDate(star, end) {
    const dateStar = moment(star);
    const dateEnd = moment(end);
    return this.dataSource.data.filter(f => {
      const date = moment(f.date);
      return date >= dateStar && date <= dateEnd;
    });
  }
}
