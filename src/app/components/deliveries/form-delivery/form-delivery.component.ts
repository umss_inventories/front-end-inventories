import {Component, Inject, OnInit} from '@angular/core';
import {Product} from '../../../model/products';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ProductsService} from '../../../services/products.service';
import {AuthService} from '../../../services/auth.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {date} from '../../../services/Tools';
import {PersonalService} from '../../../services/personal.service';
import {Personal} from '../../../model/personal';
import {Delivery} from '../../../model/delivery';

@Component({
  selector: 'app-form-delivery',
  templateUrl: './form-delivery.component.html',
  styleUrls: ['./form-delivery.component.scss']
})
export class FormDeliveryComponent implements OnInit {
  products: Product[] = [];
  personals: Personal[] = [];
  form: FormGroup;
  productMax = 0;
  constructor(private productService: ProductsService,
              private personalService: PersonalService,
              private fb: FormBuilder,
              private authUser: AuthService,
              public dialogRef: MatDialogRef<FormDeliveryComponent>,
              @Inject(MAT_DIALOG_DATA) public data: Delivery) {
    this.form = this.fb.group({
        id: this.data.id,
        id_user: [this.data.id_user ? this.data.id_user : this.authUser.getUser().id],
        id_product: [this.data.id_product, [Validators.required]],
        id_personal: [this.data.id_personal, [Validators.required]],
        quantity: [Math.abs(this.data.quantity), [Validators.required]],
      date: [this.data.date ? this.data.date : new Date(), Validators.required],
      }
    );
  }

  ngOnInit() {
    this.productService.getIndex().subscribe(res => {
      this.products = res['products'];
    });
    this.personalService.getIndex().subscribe(res => {
      this.personals = res['personals'];
    });
  }
  create() {
    const vat = this.form.value;
    vat['date'] = date(this.form.value['date']);
    this.dialogRef.close(this.form.value);
  }
  close() {
    this.dialogRef.close();
  }

  setProductMax(evt) {
    const val = this.products.find(r => {
      return r.id === evt;
    });
    this.productMax = val.quantity;
  }

  isDisabled() {
    return this.form.get('quantity').value > this.productMax || this.form.get('quantity').value < 0;
  }
}
