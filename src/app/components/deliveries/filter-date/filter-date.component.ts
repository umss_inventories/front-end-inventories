import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatDialogRef} from '@angular/material';
import {date} from '../../../services/Tools';

@Component({
  selector: 'app-filter-date',
  templateUrl: './filter-date.component.html',
  styleUrls: ['./filter-date.component.scss']
})
export class FilterDateComponent implements OnInit {
  form: FormGroup;

  constructor(private fb: FormBuilder,
              public dialogRef: MatDialogRef<FilterDateComponent>) {
    this.form = this.fb.group({
        dateStar: [Validators.required],
        dateEnd: [Validators.required],
      }
    );
  }

  ngOnInit() {
  }

  create() {
    this.form.value['dateStar'] = date(this.form.value['dateStar']);
    this.form.value['dateEnd'] = date(this.form.value['dateEnd']);
    console.log(this.form.value);
    this.dialogRef.close(this.form.value);
  }

  close() {
    this.dialogRef.close();
  }
}
