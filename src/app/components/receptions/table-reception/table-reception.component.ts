import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';
import {SideNavService} from '../../../services/side-nav.service';
import {SnackSuccessComponent} from '../../dialogs/snack-success/snack-success.component';
import {SnackErrorComponent} from '../../dialogs/snack-error/snack-error.component';
import {DeleteDialogComponent} from '../../dialogs/delete-dialog/delete-dialog.component';
import {ReceptionService} from '../../../services/reception.service';
import {Reception} from '../../../model/reception';
import {FormReceptionComponent} from '../form-reception/form-reception.component';
import {Crud} from '../../crud';
import {SnackDeleteComponent} from '../../dialogs/snack-delete/snack-delete.component';
import {FilterDateComponent} from '../../deliveries/filter-date/filter-date.component';
import * as moment from 'moment';
import {PdfService} from '../../../services/pdf.service';

@Component({
  selector: 'app-table-reception',
  templateUrl: './table-reception.component.html',
  styleUrls: ['./table-reception.component.scss']
})
export class TableReceptionComponent implements Crud, OnInit {
  displayedColumns: string[] = ['position', 'product', 'unity', 'quantity', 'name', 'date', 'option'];
  title = 'Recepciones';
  dataSource = new MatTableDataSource<Reception>([]);
  loader = false;
  receptionSelect: Reception;
  private pageIndex = 0;
  private pageSize = 10;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  constructor(private receptionService: ReceptionService,
              private sideService:  SideNavService,
              private snack: MatSnackBar,
              public dialog: MatDialog,
              private pdf: PdfService) {
  }
  ngOnInit() {
    this.loader = true;
    this.receptionService.getIndex().subscribe(res => {
      this.dataSource = new MatTableDataSource<Reception>(res['receptions']);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.loader = false;
    });
  }
  newReception() {
    const dialogRef = this.dialog.open(FormReceptionComponent, {
      width: '300px',
      data: new Reception()
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.receptionSelect = result;
        this.store();
      }
    });
  }
  count(pos) {
    return pos + (this.pageSize * this.pageIndex) + 1;
  }
  setProduct(p: Reception) {
    this.receptionSelect = p;
  }
  edit() {
    const dialogRef = this.dialog.open(FormReceptionComponent, {
      width: '300px',
      data: this.receptionSelect
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.receptionSelect = result;
        this.store();
      }
    });
  }
  store() {
    this.loader = true;
    if (!this.receptionSelect.id) {
      this.receptionService.create(this.receptionSelect).subscribe(res => {
        this.ngOnInit();
        this.snack.openFromComponent(SnackSuccessComponent, {duration: 2000});
      }, error1 => {
        this.loader = false;
        this.snack.openFromComponent(SnackErrorComponent, {duration: 2000});
      });
    } else {
      this.receptionService.update(this.receptionSelect, this.receptionSelect.id).subscribe(res => {
        this.ngOnInit();
        this.snack.openFromComponent(SnackSuccessComponent, {duration: 2000});
      }, error1 => {
        this.loader = false;
        this.snack.openFromComponent(SnackErrorComponent, {duration: 2000});
      });
    }
  }
  delete() {
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '300px',
      data: this.receptionSelect.product
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.receptionService.destroy(this.receptionSelect.id).subscribe(res => {
          this.ngOnInit();
          this.snack.openFromComponent(SnackDeleteComponent, {duration: 2000});
        }, error1 => {
          this.loader = false;
          this.snack.openFromComponent(SnackErrorComponent, {duration: 2000});
        });
      }
    });
  }

  sety(t) {
    this.pageIndex = t.pageIndex;
    this.pageSize = t.pageSize;
  }

  filter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  filterDate() {
    const dialogRef = this.dialog.open(FilterDateComponent, {
      width: '300px',
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.print(this.filterOnDate(result.dateStar, result.dateEnd));
      }
    });
  }

  filterOnDate(star, end) {
    const dateStar = moment(star);
    const dateEnd = moment(end);
    return this.dataSource.data.filter(f => {
      const date = moment(f.date);
      return date >= dateStar && date <= dateEnd;
    });
  }

  print(data) {
    const head = ['NRO', 'PRODUCTO', 'UNIDAD', 'CANTIDAD', 'REGISTRADO POR', 'FECHA'];
    const body = this.getData(data);
    this.pdf.createSingleTable('REPORTE DE RECEPCIONES', head, body, null,
      {
        0: {
          halign: 'left',
          // columnWidth: 5
        },
        1: {
          // columnWidth: 10
        },
        2: {
          // columnWidth: 20
        },
        3: {
          halign: 'right',
          cellWidth: 10
        },
        6: {
          halign: 'right',
          // columnWidth: 20
        },
      });
    this.pdf.printPdf();
  }

  getData(arrayData: any[]) {
    const data = [];
    arrayData.forEach((c, index) => {
      const d = [];
      d.push(index + 1);
      d.push(c.product.toUpperCase());
      d.push(c.unity);
      d.push(Math.abs(c.quantity));
      d.push(c.name);
      d.push(c.date);
      data.push(d);
    });
    return data;
  }
}
