import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Product} from '../../../model/products';
import {Reception} from '../../../model/reception';
import {ProductsService} from '../../../services/products.service';
import {AuthService} from '../../../services/auth.service';
import {date} from '../../../services/Tools';

@Component({
  selector: 'app-form-reception',
  templateUrl: './form-reception.component.html',
  styleUrls: ['./form-reception.component.scss']
})
export class FormReceptionComponent implements OnInit {
  products: Product[] = [];
  form: FormGroup;
  constructor(private productService: ProductsService,
              private fb: FormBuilder,
              private authUser: AuthService,
              public dialogRef: MatDialogRef<FormReceptionComponent>,
              @Inject(MAT_DIALOG_DATA) public data: Reception) {
    this.form = this.fb.group({
        id: this.data.id,
        id_user: [this.data.id_user ? this.data.id_user : this.authUser.getUser().id],
        id_product: [this.data.id_product, [Validators.required]],
        quantity: [Math.abs(this.data.quantity), [Validators.required]],
      date: [this.data.date ? this.data.date : new Date(), Validators.required],
      }
    );
  }

  ngOnInit() {
    this.productService.getIndex().subscribe(res => {
      this.products = res['products'];
    });
  }
  create() {
    const vat = this.form.value;
    vat['date'] = date(this.form.value['date']);
    this.dialogRef.close(this.form.value);
  }
  close() {
    this.dialogRef.close();
  }

}
