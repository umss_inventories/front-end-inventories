import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatDialogRef} from '@angular/material';
import {ɵDomAdapter} from '@angular/platform-browser';

@Component({
  selector: 'app-dialog-report',
  templateUrl: './dialog-report.component.html',
  styleUrls: ['./dialog-report.component.scss']
})
export class DialogReportComponent implements OnInit {
  type;
  form: FormGroup;
  seasons = [
    {value: 1, text: 'Ingresos de Productos'},
    {value: 2, text: 'Egresos de Productos'},
    {value: 3, text: 'Productos Existentes'}
  ];

  constructor(private fb: FormBuilder,
              public dialogRef: MatDialogRef<DialogReportComponent>) {
    this.form = this.fb.group({
        dateStar: null,
        dateEnd: null,
        type: null
      }
    );
  }

  ngOnInit() {
  }

  accept() {
    this.dialogRef.close(this.form.value);
  }

  isDate() {
    const t = this.form.get('type').value;
    return t === 1 || t === 2;
  }

  isDisabled() {
    const type = this.form.get('type').value;
    const date1 = this.form.get('dateStar').value;
    const date2 = this.form.get('dateEnd').value;
    if (type === 1 || type === 2) {
      return date1 && date2;
    } else {
      return !!type;
    }
  }
}
