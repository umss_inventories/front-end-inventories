import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UnityService} from '../../../services/unity.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Inventory} from '../../../model/inventory';

@Component({
  selector: 'app-form-inventory',
  templateUrl: './form-inventory.component.html',
  styleUrls: ['./form-inventory.component.scss']
})
export class FormInventoryComponent implements OnInit {
  form: FormGroup;

  constructor(private unityService: UnityService,
              private fb: FormBuilder,
              public dialogRef: MatDialogRef<FormInventoryComponent>,
              @Inject(MAT_DIALOG_DATA) public data: Inventory) {
    this.form = this.fb.group({
        id: this.data.id,
        name: [this.data.name, [Validators.required]],
      }
    );
  }

  ngOnInit() {
  }

  create() {
    this.dialogRef.close(this.form.value);
  }

  close() {
    this.dialogRef.close(null);
  }
}
