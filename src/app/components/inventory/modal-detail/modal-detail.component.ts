import {Component, OnInit, ViewChild} from '@angular/core';
import {ProductsService} from '../../../services/products.service';
import {PersonalService} from '../../../services/personal.service';
import {AuthService} from '../../../services/auth.service';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {Product} from '../../../model/products';
import {Personal} from '../../../model/personal';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-modal-detail',
  templateUrl: './modal-detail.component.html',
  styleUrls: ['./modal-detail.component.scss']
})
export class ModalDetailComponent implements OnInit {
  displayedColumns: string[] = ['position', 'name'];
  title = 'Funcionarios';
  products: MatTableDataSource<Product> = new MatTableDataSource<Product>([]);
  personals: MatTableDataSource<Personal> = new MatTableDataSource<Personal>([]);
  loader = false;
  private pageIndex = 0;
  private pageSize = 10;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private productService: ProductsService,
              private personalService: PersonalService,
              private authUser: AuthService,
              private router: ActivatedRoute) {
  }

  ngOnInit() {
    this.loader = true;
    this.title = 'Personal Departamento ' + sessionStorage.getItem('inventory');
    this.router.params.subscribe(params => {
      this.personalService.getIndex(params.id).subscribe(res => {
        this.personals = res['personals'];
        this.personals = new MatTableDataSource<Personal>(res['personals']);
        this.personals.sort = this.sort;
        this.personals.paginator = this.paginator;
        this.loader = false;
      });
    });
  }

  count(pos) {
    return pos + (this.pageSize * this.pageIndex) + 1;
  }

  sety(t) {
    this.pageIndex = t.pageIndex;
    this.pageSize = t.pageSize;
  }

  filter(filterValue: string) {
    this.personals.filter = filterValue.trim().toLowerCase();
  }
}
