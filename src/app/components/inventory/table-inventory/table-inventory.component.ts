import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';
import {SideNavService} from '../../../services/side-nav.service';
import {FormPersonalComponent} from '../../personaal/form-personal/form-personal.component';
import {SnackSuccessComponent} from '../../dialogs/snack-success/snack-success.component';
import {SnackErrorComponent} from '../../dialogs/snack-error/snack-error.component';
import {DeleteDialogComponent} from '../../dialogs/delete-dialog/delete-dialog.component';
import {SnackDeleteComponent} from '../../dialogs/snack-delete/snack-delete.component';
import {Crud} from '../../crud';
import {Inventory} from '../../../model/inventory';
import {InventoryService} from '../../../services/inventory.service';
import {FormInventoryComponent} from '../form-inventory/form-inventory.component';
import {Router} from '@angular/router';
import {AuthService} from '../../../services/auth.service';

@Component({
  selector: 'app-table-inventory',
  templateUrl: './table-inventory.component.html',
  styleUrls: ['./table-inventory.component.scss']
})
export class TableInventoryComponent implements Crud, OnInit {
  displayedColumns: string[] = ['position', 'name', 'option'];
  title = 'Departamentos';
  dataSource = new MatTableDataSource<Inventory>([]);
  loader = false;
  inventorySelect: Inventory;
  private pageIndex = 0;
  private pageSize = 10;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private inventoryService: InventoryService,
              private sideService: SideNavService,
              private snack: MatSnackBar,
              public dialog: MatDialog,
              private router: Router,
              public auth: AuthService) {
  }

  ngOnInit() {
    this.loader = true;
    this.inventoryService.getIndex().subscribe(res => {
      this.dataSource = new MatTableDataSource<Inventory>(res['inventories']);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.loader = false;
    });
  }

  newInventory() {
    const dialogRef = this.dialog.open(FormInventoryComponent, {
      width: '300px',
      data: new Inventory()
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.inventorySelect = result;
        this.store();
      }
    });
  }

  count(pos) {
    return pos + (this.pageSize * this.pageIndex) + 1;
  }

  setProduct(p: Inventory) {
    this.inventorySelect = p;
  }

  edit() {
    const dialogRef = this.dialog.open(FormPersonalComponent, {
      width: '300px',
      data: this.inventorySelect
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.inventorySelect = result;
        this.store();
      }
    });
  }

  store() {
    this.loader = true;
    if (!this.inventorySelect.id) {
      this.inventoryService.create(this.inventorySelect).subscribe(res => {
        this.ngOnInit();
        this.snack.openFromComponent(SnackSuccessComponent, {duration: 2000});
      }, error1 => {
        this.loader = false;
        this.snack.openFromComponent(SnackErrorComponent, {duration: 2000});
      });
    } else {
      this.inventoryService.update(this.inventorySelect, this.inventorySelect.id).subscribe(res => {
        this.ngOnInit();
        this.snack.openFromComponent(SnackSuccessComponent, {duration: 2000});
      }, error1 => {
        this.loader = false;
        this.snack.openFromComponent(SnackErrorComponent, {duration: 2000});
      });
    }
  }

  delete() {
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '300px',
      data: this.inventorySelect.name
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.inventoryService.destroy(this.inventorySelect.id).subscribe(res => {
          this.ngOnInit();
          this.snack.openFromComponent(SnackDeleteComponent, {duration: 2000});
        }, error1 => {
          this.loader = false;
          this.snack.openFromComponent(SnackErrorComponent, {duration: 2000});
        });
      }
    });
  }


  sety(t) {
    this.pageIndex = t.pageIndex;
    this.pageSize = t.pageSize;
  }

  filter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  detail() {
    sessionStorage.setItem('inventory', this.inventorySelect.name);
    this.router.navigate(['supervisor/department/' + this.inventorySelect.id]);
  }

  detailInventory() {
    sessionStorage.setItem('inventory', this.inventorySelect.name);
    this.router.navigate(['supervisor/inventory/' + this.inventorySelect.id]);
  }
}
