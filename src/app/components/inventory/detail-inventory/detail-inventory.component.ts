import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';
import {Product} from '../../../model/products';
import {ProductsService} from '../../../services/products.service';
import {SideNavService} from '../../../services/side-nav.service';
import {ActivatedRoute} from '@angular/router';
import {PdfService} from '../../../services/pdf.service';
import {DialogReportComponent} from '../dialog-report/dialog-report.component';
import {DeliveryService} from '../../../services/delivery.service';
import {ReceptionService} from '../../../services/reception.service';
import {Reception} from '../../../model/reception';
import * as moment from 'moment';
import {Delivery} from '../../../model/delivery';

@Component({
  selector: 'app-detail-inventory',
  templateUrl: './detail-inventory.component.html',
  styleUrls: ['./detail-inventory.component.scss']
})
export class DetailInventoryComponent implements OnInit {
  displayedColumns: string[] = ['position', 'name', 'unity', 'quantity'];
  title = 'Productos';
  dataSource = new MatTableDataSource<Product>([]);
  loader = false;
  private pageIndex = 0;
  private pageSize = 10;
  id;
  auxData: any[];
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private pService: ProductsService,
              private delivery: DeliveryService,
              private recepit: ReceptionService,
              private sideService: SideNavService,
              private snack: MatSnackBar,
              public dialog: MatDialog,
              private router: ActivatedRoute,
              private pdf: PdfService) {
  }

  ngOnInit() {
    this.loader = true;
    this.title = 'Productos Departamento ' + sessionStorage.getItem('inventory');
    this.router.params.subscribe(params => {
      this.id = params.id;
      this.pService.getIndex(params.id).subscribe(res => {
        this.dataSource = new MatTableDataSource<Product>(res['products']);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.loader = false;
      });
    });
  }

  count(pos) {
    return pos + (this.pageSize * this.pageIndex) + 1;
  }

  printF() {
    const dg = this.dialog.open(DialogReportComponent, {
      width: '300px',
    });
    dg.afterClosed().subscribe(r => {
      if (r) {
        // dateStar: null,
        // dateEnd: null,
        // type: null
        if (r.type === 3) {
          this.print(this.getData(this.dataSource.data),
            ['NRO', 'PRODUCTO', 'CANTIDAD', 'UNIDAD'],
            'REPORTE DE PRODUCTOS');
        } else if (r.type === 1) {
          this.recepit.getIndex(this.id).subscribe(re => {
            this.auxData = <Reception[]>re['receptions'];
            this.print(this.getReception(this.filterOnDate(this.auxData, r.dateStar, r.dateEnd)),
              ['NRO', 'PRODUCTO', 'UNIDAD', 'CANTIDAD', 'REGISTRADO POR', 'FECHA'],
              'REPORTE DE RECEPCIONES');
          });
        } else if (r.type === 2) {
          this.delivery.getIndex(this.id).subscribe(re => {
            this.auxData = <Delivery[]>re['deliveries'];
            this.print(this.getDelivery(this.filterOnDate(this.auxData, r.dateStar, r.dateEnd)),
              ['NRO', 'PRODUCTO', 'FUNCIONARIO', 'ENTREGADO', 'UNIDAD', 'FECHA', 'FIRMA'],
              'REPORTE DE ENTREGAS');
          });
        }
      }
    });
  }

  print(data, head, title) {
    this.pdf.createSingleTable(title, head, data, null,
      {
        0: {
          halign: 'left',
          // columnWidth: 5
        },
        1: {
          // columnWidth: 10
        },
        2: {
          // columnWidth: 20
        },
        3: {
          halign: 'right',
          cellWidth: 10
        },
      });
    this.pdf.printPdf();
  }

  getDelivery(arrayData: Delivery[]) {
    const data = [];
    arrayData.forEach((c, index) => {
      const d = [];
      d.push(index + 1);
      d.push(c.product.toUpperCase());
      d.push(c.user_personal.toUpperCase());
      d.push(Math.abs(c.quantity));
      d.push(c.unity);
      d.push(c.date);
      d.push('');
      data.push(d);
    });
    return data;
  }

  getReception(arrayData: Reception[]) {
    const data = [];
    arrayData.forEach((c, index) => {
      const d = [];
      d.push(index + 1);
      d.push(c.product.toUpperCase());
      d.push(c.unity);
      d.push(Math.abs(c.quantity));
      d.push(c.name);
      d.push(c.date);
      data.push(d);
    });
    return data;
  }
  getData(arrayData: Product[]) {
    const data = [];
    arrayData.forEach((c, index) => {
      const d = [];
      d.push(index + 1);
      d.push(c.name.toUpperCase());
      d.push(c.quantity);
      d.push(c.unity.toUpperCase());
      data.push(d);
    });
    return data;
  }

  sety(t) {
    this.pageIndex = t.pageIndex;
    this.pageSize = t.pageSize;
  }

  filter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  filterOnDate(data: any[], star, end) {
    const dateStar = moment(star);
    const dateEnd = moment(end);
    return data.filter(f => {
      const date = moment(f.date);
      return date >= dateStar && date <= dateEnd;
    });
  }
}
