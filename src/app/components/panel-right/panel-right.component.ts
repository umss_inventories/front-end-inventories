import { Component, OnInit } from '@angular/core';
import {SideNavService} from '../../services/side-nav.service';

@Component({
  selector: 'app-panel-right',
  templateUrl: './panel-right.component.html',
  styleUrls: ['./panel-right.component.scss']
})
export class PanelRightComponent implements OnInit {

  constructor(public sideService: SideNavService) { }

  ngOnInit() {
  }
  created(evt) {
    console.log(evt);
  }
}
