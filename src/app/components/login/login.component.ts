import { Component, OnInit } from '@angular/core';
import {SideNavService} from '../../services/side-nav.service';
import {AuthService} from '../../services/auth.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatSnackBar} from '@angular/material';
import {Router} from '@angular/router';
import {HttpErrorResponse} from '@angular/common/http';
import {SnackErrorComponent} from '../dialogs/snack-error/snack-error.component';
import {getItem} from '../../services/session';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  password = 'password';
  loader = false;

  constructor(private sideService: SideNavService,
              private authService: AuthService,
              private fb: FormBuilder,
              private snack: MatSnackBar, private router: Router) {
    this.form = this.fb.group({
        email: ['', [Validators.required]],
        password: ['', Validators.required],
      }
    );
  }

  ngOnInit() {
    this.sideService.closeAll();
  }
  login() {
    this.loader = true;
    this.authService.login(this.form.value).subscribe(res => {
      // tslint:disable-next-line:triple-equals
      if (res.user.pass_no_change == 1) {
        this.router.navigate(['/reLogin']);
      }
      if (this.authService.getRol() === 'Root' || this.authService.getRol() === 'Supervisor') {
        this.router.navigate([`/root/inventories`]);
      } else if (this.authService.getRol() === 'Administrador') {
        this.router.navigate([`/admin/${getItem('inventory').id}/products`]);
      }
      this.sideService.sideLeft = true;
      this.loader = false;
    }, (error: HttpErrorResponse) => {
      this.loader = false;
      if (error.status === 401) {
        this.snack.openFromComponent(SnackErrorComponent, {duration: 2000, data: 'Usuario o Contraseña Incorrecta'});
      } else {
        this.snack.openFromComponent(SnackErrorComponent, {duration: 2000});
      }
    });
  }
}
