export interface Crud {
  store();
  edit();
  delete();
}
