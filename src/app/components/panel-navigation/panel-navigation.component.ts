import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';
import {User} from '../../model/user';
import {SideNavService} from '../../services/side-nav.service';
import {getItem} from '../../services/session';
import {MatDialog} from '@angular/material';
import {SetPassComponent} from '../users/set-pass/set-pass.component';
import {UserService} from '../../services/user.service';

@Component({
  selector: 'app-panel-navigation',
  templateUrl: './panel-navigation.component.html',
  styleUrls: ['./panel-navigation.component.scss']
})
export class PanelNavigationComponent implements OnInit {
  user: User = {
    name: '',
    id_rol: null,
    id_inventory: null,
    email: '',
    id: null,
    department: ''
  };
  items = [];
  private routes = [
    {name: 'Productos', icon: 'folder', route: '/products'},
    {name: 'Recepciones', icon: 'add_box', route: '/receptions'},
    {name: 'Despachos', icon: 'indeterminate_check_box', route: '/deliveries'},
    {name: 'Unidades', icon: 'ballot', route: '/unities'},
    {name: 'Funcionarios', icon: 'group', route: '/personal'},
    {name: 'Departamentos', icon: 'kitchen', route: '/inventories'},
    {name: 'Usuarios', icon: 'verified_user', route: '/users'}
    ];


  constructor(public authService: AuthService,
              private router: Router,
              public sideService: SideNavService,
              private userS: UserService,
              private dg: MatDialog) {
  }

  ngOnInit() {
    this.user = this.authService.getUser();
    if (this.authService.getRol() === 'Root' || this.authService.getRol() === 'Supervisor') {
      this.loadItemRoot();
    }
    if (this.authService.getRol() === 'Administrador') {
      this.loadItemAdmin();
    }
  }
  logout() {
    this.authService.logout().subscribe(res => {
      // if (res === 'logout') {
      this.router.navigate(['/login']);
      this.sideService.closeAll();
      // }
    });
  }

  loadItemRoot() {
    this.items = [];
    this.routes.forEach(r => {
      if (r.name === 'Departamentos' || r.name === 'Usuarios') {
        r.route = '/root/' + r.route;
        this.items.push(r);
      }
    });
  }

  loadItemAdmin() {
    this.items = [];
    this.routes.forEach(r => {
      if (r.name !== 'Departamentos') {
        r.route = `/admin/${btoa(getItem('inventory').id)}/${r.route}`;
        this.items.push(r);
      }
    });
  }

  open() {
    const r = this.dg.open(SetPassComponent, {
      width: '300px',
      data: this.authService.getUser()
    });
    r.afterClosed().subscribe(p => {
      if (p) {
        this.userS.resetPass(p).subscribe(rea => {
          this.authService.logout().subscribe(end => {
            this.router.navigate(['/login']);
            this.sideService.closeAll();
          });
        });
      }
    });
  }
}
