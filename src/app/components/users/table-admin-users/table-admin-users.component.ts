import {Component, OnInit} from '@angular/core';
import {TableUsersComponent} from '../table-users/table-users.component';
import {MatDialog, MatSnackBar, MatTableDataSource} from '@angular/material';
import {User} from '../../../model/user';
import {FormUserComponent} from '../form-user/form-user.component';
import {UserService} from '../../../services/user.service';
import {SideNavService} from '../../../services/side-nav.service';

@Component({
  selector: 'app-table-admin-users',
  templateUrl: './table-admin-users.component.html',
  styleUrls: ['./table-admin-users.component.scss']
})
export class TableAdminUsersComponent extends TableUsersComponent {
  displayedColumns: string[] = ['position', 'name', 'email', 'option'];
  // constructor(private userService: UserService,
  //             private sideService: SideNavService,
  //             private snack: MatSnackBar,
  //             public dialog: MatDialog) {
  //   super(userService,
  //     sideService,
  //     snack,
  //     dialog);
  // }
  // ngOnInit() {
  //   this.loader = true;
  //   this.sideService.openLeft();
  //   this.userService.getIndex().subscribe(res => {
  //     this.dataSource = new MatTableDataSource<User>(res['users']);
  //     this.dataSource.paginator = this.paginator;
  //     this.dataSource.sort = this.sort;
  //     this.loader = false;
  //   });
  // }

  newProduct() {
    const dialogRef = this.dialog.open(FormUserComponent, {
      width: '300px',
      data: new User()
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.userSelect = result;
        this.store();
      }
    });
  }
}
