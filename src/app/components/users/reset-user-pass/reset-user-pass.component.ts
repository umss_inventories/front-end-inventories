import {Component, Inject, OnInit} from '@angular/core';
import {Personal} from '../../../model/personal';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {PersonalService} from '../../../services/personal.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {User} from '../../../model/user';

@Component({
  selector: 'app-reset-user-pass',
  templateUrl: './reset-user-pass.component.html',
  styleUrls: ['./reset-user-pass.component.scss']
})
export class ResetUserPassComponent implements OnInit {

  personals: Personal[] = [];
  form: FormGroup;

  constructor(private personalService: PersonalService,
              private fb: FormBuilder,
              public dialogRef: MatDialogRef<ResetUserPassComponent>,
              @Inject(MAT_DIALOG_DATA) public data: User) {
    this.form = this.fb.group({
        id: [data.id, Validators.required],
        name: new FormControl({value: this.data.name, disabled: true}),
        email: new FormControl({value: this.data.email, disabled: true}),
        password: ['@umss_abc_123', [Validators.required, Validators.minLength(8)]],
      }
    );
  }

  ngOnInit() {
    // this.personalService.getIndex().subscribe(res => {
    //   this.personals = res['personals'];
    // });
  }

  create() {
    this.dialogRef.close(this.form.value);
  }

  passEquals() {
    return this.form.controls.password.value !== this.form.controls.confirm_password.value;
  }

  close() {
    this.dialogRef.close();
  }
}
