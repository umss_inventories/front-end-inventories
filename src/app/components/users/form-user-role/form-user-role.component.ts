import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {User} from '../../../model/user';
import {Inventory} from '../../../model/inventory';
import {InventoryService} from '../../../services/inventory.service';
import {UserService} from '../../../services/user.service';

@Component({
  selector: 'app-form-user-role',
  templateUrl: './form-user-role.component.html',
  styleUrls: ['./form-user-role.component.scss']
})
export class FormUserRoleComponent implements OnInit {
  inventories: Inventory[] = [];
  form: FormGroup;
  role = 'Administrador';
  users: User[];
  roles = [
    {id: 2, name: 'Administrador'},
    {id: 3, name: 'Supervisor'}
  ];

  constructor(private inventoryService: InventoryService,
              private userService: UserService,
              private fb: FormBuilder,
              public dialogRef: MatDialogRef<FormUserRoleComponent>,
              @Inject(MAT_DIALOG_DATA) public data: User) {
    this.form = this.fb.group({
      id: this.data.id,
      id_inventory: data.id_inventory,
      id_rol: [this.data.id_rol, [Validators.required]],
      name: [this.data.name, [Validators.required]],
      email: [this.data.email, [Validators.required, Validators.email]],
      password: ['@umss_abc_123', [Validators.required, Validators.minLength(7)]],
      // confirm_password: [null, [Validators.required, Validators.minLength(7)]],
      }
    );
  }

  ngOnInit() {
    this.inventoryService.getIndex().subscribe(res => {
      this.inventories = res['inventories'];
    });
    this.userService.getIndex().subscribe(res => {
      this.users = res['users'];
    });
  }

  create() {
    if (this.form.controls.id_rol.value === 3) {
      this.form.controls.id_inventory.setValue(null);
    }
    this.dialogRef.close(this.form.value);
  }

  close() {
    this.dialogRef.close();
  }

  passEquals() {
    return this.form.controls.password.value !== this.form.controls.confirm_password.value;
  }

  idAdmin() {
    return this.form.controls.id_rol.value === 2;
  }

  isRequired() {
    return this.form.controls.id_rol.value === 2;
  }
}
