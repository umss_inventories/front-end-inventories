import {Component, Inject, OnInit} from '@angular/core';
import {PersonalService} from '../../../services/personal.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Personal} from '../../../model/personal';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {User} from '../../../model/user';
import {getItem} from '../../../services/session';
import {AuthService} from '../../../services/auth.service';

@Component({
  selector: 'app-form-user',
  templateUrl: './form-user.component.html',
  styleUrls: ['./form-user.component.scss']
})
export class FormUserComponent implements OnInit {
  personals: Personal[] = [];
  form: FormGroup;

  constructor(private personalService: PersonalService,
              private fb: FormBuilder,
              public dialogRef: MatDialogRef<FormUserComponent>,
              @Inject(MAT_DIALOG_DATA) public data: User,
              private auth: AuthService) {
    this.form = this.fb.group({
      id: this.data.id,
      name: [this.data.name, [Validators.required]],
      email: [this.data.email, [Validators.required]],
      password: ['@inventario@', [Validators.required]],
      // confirm_password: [null, [Validators.required]],
      }
    );
  }

  ngOnInit() {
    // if (this.data.id) {
    // this.form = this.fb.group({
    //     id: this.data.id,
    //     name: [this.data.name, [Validators.required]],
    //     email: [this.data.email, [Validators.required]],
    //     password: [null, [Validators.required]],
    //     confirm_password: [null, [Validators.required]],
    //   }
    // );
    // }
    // this.personalService.getIndex().subscribe(res => {
    //   this.personals = res['personals'];
    // });
  }

  create() {
    const user = this.form.value;
    if (this.auth.getInventory()) {
      user['id_inventory'] = this.auth.getInventory().id;
      user['id_rol'] = this.auth.getUser().id_rol;
    }
    this.dialogRef.close(user);
  }

  close() {
    this.dialogRef.close();
  }
}
