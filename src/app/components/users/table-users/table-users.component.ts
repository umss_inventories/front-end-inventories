import {Component, OnInit, ViewChild} from '@angular/core';
import {Crud} from '../../crud';
import {MatDialog, MatPaginator, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';
import {SideNavService} from '../../../services/side-nav.service';
import {SnackSuccessComponent} from '../../dialogs/snack-success/snack-success.component';
import {SnackErrorComponent} from '../../dialogs/snack-error/snack-error.component';
import {User} from '../../../model/user';
import {UserService} from '../../../services/user.service';
import {FormUserComponent} from '../form-user/form-user.component';
import {AuthService} from '../../../services/auth.service';
import {FormUserRoleComponent} from '../form-user-role/form-user-role.component';
import {DeleteDialogComponent} from '../../dialogs/delete-dialog/delete-dialog.component';
import {SnackDeleteComponent} from '../../dialogs/snack-delete/snack-delete.component';
import {ResetUserPassComponent} from '../reset-user-pass/reset-user-pass.component';

@Component({
  selector: 'app-table-users',
  templateUrl: './table-users.component.html',
  styleUrls: ['./table-users.component.scss']
})
export class TableUsersComponent implements Crud, OnInit {

  displayedColumns: string[] = ['position', 'name', 'rol', 'department', 'email', 'option'];
  title = 'Usuarios';
  dataSource = new MatTableDataSource<User>([]);
  loader = false;
  userSelect: User;
  private pageIndex = 0;
  private pageSize = 10;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private userService: UserService,
              private sideService: SideNavService,
              private authService: AuthService,
              private snack: MatSnackBar,
              public dialog: MatDialog) {
  }

  ngOnInit() {
    this.loader = true;
    if (this.authService.getRol() === 'Root' || this.authService.getRol() === 'Supervisor') {
      this.userService.getIndex().subscribe(res => {
        this.dataSource = new MatTableDataSource<User>(res['users']);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.loader = false;
      });
    }
    if (this.authService.getRol() === 'Administrador') {
      this.userService.getIndexInventory().subscribe(res => {
        this.dataSource = new MatTableDataSource<User>(res['users']);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.loader = false;
      });
    }
  }

  newProduct() {
    const dialogRef = this.authService.getRol() === 'Root' ?
      this.dialog.open(FormUserRoleComponent, {
        width: '300px',
        data: new User()
      }) :
      this.dialog.open(FormUserComponent, {
        width: '300px',
        data: new User()
      });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.userSelect = result;
        this.store();
      }
    });
  }

  count(pos) {
    return pos + (this.pageSize * this.pageIndex) + 1;
  }

  setType(p: User) {
    this.userSelect = p;
  }

  edit() {
    const dialogRef = this.dialog.open(FormUserComponent, {
      width: '300px',
      data: this.userSelect
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.userSelect = result;
        this.store();
      }
    });
  }

  store() {
    this.loader = true;
    if (!this.userSelect.id) {
      this.userService.create(this.userSelect).subscribe(res => {
        this.ngOnInit();
        this.snack.openFromComponent(SnackSuccessComponent, {duration: 2000});
      }, error1 => {
        this.loader = false;
        this.snack.openFromComponent(SnackErrorComponent, {duration: 2000});
      });
    } else {
      this.userService.update(this.userSelect, this.userSelect.id).subscribe(res => {
        this.ngOnInit();
        this.snack.openFromComponent(SnackSuccessComponent, {duration: 2000});
      }, error1 => {
        this.loader = false;
        this.snack.openFromComponent(SnackErrorComponent, {duration: 2000});
      });
    }
  }

  delete() {
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '300px',
      data: this.userSelect.name
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.userService.destroy(this.userSelect.id).subscribe(res => {
          this.ngOnInit();
          this.snack.openFromComponent(SnackDeleteComponent, {duration: 2000});
        }, error1 => {
          this.loader = false;
          this.snack.openFromComponent(SnackErrorComponent, {duration: 2000});
        });
      }
    });
  }

  sety(t) {
    this.pageIndex = t.pageIndex;
    this.pageSize = t.pageSize;
  }

  getUser() {
    return this.authService.getRol() === 'Root';
  }

  reset() {
    const dialogRef = this.dialog.open(ResetUserPassComponent, {
      width: '300px',
      data: this.userSelect
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.userService.reset(result, this.userSelect.id).subscribe(res => {
          this.ngOnInit();
          this.snack.openFromComponent(SnackDeleteComponent, {duration: 2000});
        }, error1 => {
          this.loader = false;
          this.snack.openFromComponent(SnackErrorComponent, {duration: 2000});
        });
      }
    });
  }
}
