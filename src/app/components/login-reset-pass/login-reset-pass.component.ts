import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {SideNavService} from '../../services/side-nav.service';
import {AuthService} from '../../services/auth.service';
import {MatSnackBar} from '@angular/material';
import {Router} from '@angular/router';
import {getItem} from '../../services/session';
import {HttpErrorResponse} from '@angular/common/http';
import {SnackErrorComponent} from '../dialogs/snack-error/snack-error.component';
import {User} from '../../model/user';
import {SnackAlertComponent} from '../dialogs/snack-alert/snack-alert.component';

@Component({
  selector: 'app-login-reset-pass',
  templateUrl: './login-reset-pass.component.html',
  styleUrls: ['./login-reset-pass.component.scss']
})
export class LoginResetPassComponent implements OnInit {

  form: FormGroup;
  lastPassword = 'password';
  newPassword = 'password';
  repeatNewPassword = 'password';
  loader = false;
  user: User = JSON.parse(localStorage.getItem('user'));

  constructor(private sideService: SideNavService,
              private authService: AuthService,
              private fb: FormBuilder,
              private snack: MatSnackBar, private router: Router) {
    this.form = this.fb.group({
        email: [this.user.email, [Validators.required]],
        lastPassword: ['', Validators.required],
        newPassword: ['', Validators.required],
        repeatNewPassword: ['', Validators.required],
      }
    );
  }

  ngOnInit() {
    this.sideService.closeAll();
  }

  login() {
    this.loader = true;
    this.authService.reLogin(this.form.value).subscribe(res => {
      this.snack.openFromComponent(SnackAlertComponent, {
        duration: 4000,
        data: 'Ha cambiado su contraseña correctamente!'
      });
      if (this.authService.getRol() === 'Root' || this.authService.getRol() === 'Supervisor') {
        this.router.navigate([`/root/inventories`]);
      } else if (this.authService.getRol() === 'Administrador') {
        this.router.navigate([`/admin/${getItem('inventory').id}/products`]);
      }
      this.sideService.sideLeft = true;
      this.loader = false;
    }, (error: HttpErrorResponse) => {
      this.loader = false;
      if (error.status === 401) {
        this.snack.openFromComponent(SnackErrorComponent, {duration: 2000, data: 'Usuario o Contraseña Incorrecta'});
      }
      if (error.status === 422) {
        this.snack.openFromComponent(SnackErrorComponent, {duration: 2000, data: 'Las contraseñas con son iguales'});
      }
    });
  }
}
