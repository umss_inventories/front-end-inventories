import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';
import {Personal} from '../../../model/personal';
import {SideNavService} from '../../../services/side-nav.service';
import {SnackSuccessComponent} from '../../dialogs/snack-success/snack-success.component';
import {SnackErrorComponent} from '../../dialogs/snack-error/snack-error.component';
import {DeleteDialogComponent} from '../../dialogs/delete-dialog/delete-dialog.component';
import {Crud} from '../../crud';
import {Unity} from '../../../model/unity';
import {UnityService} from '../../../services/unity.service';
import {FormTypesComponent} from '../form-types/form-types.component';
import {SnackDeleteComponent} from '../../dialogs/snack-delete/snack-delete.component';

@Component({
  selector: 'app-tables-types',
  templateUrl: './tables-types.component.html',
  styleUrls: ['./tables-types.component.scss']
})
export class TablesTypesComponent implements Crud, OnInit {

  displayedColumns: string[] = ['position', 'name', 'option'];
  title = 'Unidades';
  dataSource = new MatTableDataSource<Unity>([]);
  loader = false;
  unitySelect: Unity;
  private pageIndex = 0;
  private pageSize = 10;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  constructor(private unityService: UnityService,
              private sideService: SideNavService,
              private snack: MatSnackBar,
              public dialog: MatDialog) {
  }

  ngOnInit() {
    this.loader = true;
    this.unityService.getUnities().subscribe(res => {
      this.dataSource = new MatTableDataSource<Unity>(res['unities']);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.loader = false;
    });
  }

  newProduct() {
    const dialogRef = this.dialog.open(FormTypesComponent, {
      width: '300px',
      data: new Unity()
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.unitySelect = result;
        this.store();
      }
    });
  }

  count(pos) {
    return pos + (this.pageSize * this.pageIndex) + 1;
  }

  setType(p: Unity) {
    this.unitySelect = p;
  }

  edit() {
    const dialogRef = this.dialog.open(FormTypesComponent, {
      width: '300px',
      data: this.unitySelect
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.unitySelect = result;
        this.store();
      }
    });
  }

  store() {
    this.loader = true;
    if (!this.unitySelect.id) {
      this.unityService.create(this.unitySelect).subscribe(res => {
        this.ngOnInit();
        this.snack.openFromComponent(SnackSuccessComponent, {duration: 2000});
      }, error1 => {
        this.loader = false;
        this.snack.openFromComponent(SnackErrorComponent, {duration: 2000});
      });
    } else {
      this.unityService.update(this.unitySelect, this.unitySelect.id).subscribe(res => {
        this.ngOnInit();
        this.snack.openFromComponent(SnackSuccessComponent, {duration: 2000});
      }, error1 => {
        this.loader = false;
        this.snack.openFromComponent(SnackErrorComponent, {duration: 2000});
      });
    }
  }

  delete() {
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '300px',
      data: this.unitySelect.name
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.unityService.destroy(this.unitySelect.id).subscribe(res => {
          this.ngOnInit();
          this.snack.openFromComponent(SnackDeleteComponent, {duration: 2000});
        }, error1 => {
          this.loader = false;
          this.snack.openFromComponent(SnackErrorComponent, {duration: 2000});
        });
      }
    });
  }

  sety(t) {
    this.pageIndex = t.pageIndex;
    this.pageSize = t.pageSize;
  }
}
