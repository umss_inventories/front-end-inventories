import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UnityService} from '../../../services/unity.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Unity} from '../../../model/unity';

@Component({
  selector: 'app-form-types',
  templateUrl: './form-types.component.html',
  styleUrls: ['./form-types.component.scss']
})
export class FormTypesComponent implements OnInit {

  form: FormGroup;

  constructor(private unityService: UnityService,
              private fb: FormBuilder,
              public dialogRef: MatDialogRef<FormTypesComponent>,
              @Inject(MAT_DIALOG_DATA) public data: Unity) {
    this.form = this.fb.group({
        id: this.data.id,
        name: [this.data.name, [Validators.required]],
      }
    );
  }

  ngOnInit() {
  }

  create() {
    this.dialogRef.close(this.form.value);
  }

  close() {
    this.dialogRef.close(null);
  }
}
