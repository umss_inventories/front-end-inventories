import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private router: Router) {
  }
  canActivate(route: ActivatedRouteSnapshot): boolean {
    const token = sessionStorage.getItem('token');
    const role = sessionStorage.getItem('role');
    if (token && role) {
      return  true;
    } else {
      this.router.navigate(['/login']);
      return false;
    }
  }
}
