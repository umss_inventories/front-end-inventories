import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router} from '@angular/router';
import {User} from '../model/user';

@Injectable({
  providedIn: 'root'
})
export class LoginGuard implements CanActivate {
  constructor(private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot): boolean {
    const user: User = localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')) : null;
    // tslint:disable-next-line:triple-equals
    if (user.pass_no_change == 1) {
      return true;
    } else {
      this.router.navigate(['/login']);
      return false;
    }
  }
}
