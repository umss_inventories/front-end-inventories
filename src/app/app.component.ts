import {Component, ViewChild} from '@angular/core';
import {SideNavService} from './services/side-nav.service';
import {existUser} from './services/Tools';
import {MatSidenav} from '@angular/material';
import {AuthService} from './services/auth.service';
import {getUser} from './services/session';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  @ViewChild('sidenav', {static: false}) sidenav: MatSidenav;

  reason = '';

  title = 'INVENTORIES-UMSS';

  constructor(public sideService: SideNavService, public authService: AuthService) {
  }

  user() {
    return existUser();
  }

  close(reason: string) {
    this.reason = reason;
    this.sideService.open();
  }

  hidden() {
    return !getUser() ? {
      background: 'transparent',
      boxShadow: 'none'
    } : {};
  }

  existUser() {
    return !!getUser();
  }
}
