import {Injectable} from '@angular/core';
import {Crud} from '../components/crud';

@Injectable({
  providedIn: 'root'
})
export class SideNavService {
  sideLeft = false;
  sideRight = false;
  product = false;
  reception = false;
  component: Crud;
  loaderForm = false;
  constructor() { }

  isMobile() {
    return window.innerWidth <= 800;
  }
  openRight() {
    this.sideRight = true;
    this.sideLeft = false;
  }

  open() {
    this.sideLeft = !this.sideLeft;
  }
  closeAll() {
    this.sideLeft = false;
    this.sideRight = false;
  }
  openForm(isForm: string, component: Crud) {
    this.component = component;
    switch (isForm) {
      case 'product': {
        this.product = true;
        this.openRight();
        break;
      }
      case 'reception': {
        this.reception = true;
        this.openRight();
        break;
      }
    }
  }
}
