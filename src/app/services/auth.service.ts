import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {User} from '../model/user';
import {Inventory} from '../model/inventory';
import {PrincipalService} from './principal.service';
import {getItem, getToken, getUser, setItem, setToken} from './session';

@Injectable({
  providedIn: 'root'
})
export class AuthService extends PrincipalService {

  login(data): Observable<any> {
    return this.http.post<any>(`${environment.server}/login`, data).pipe( map(res => {
      // tslint:disable-next-line:triple-equals
      if (res['user'].pass_no_change == 0) {
        setToken(res['token']);
        setItem('user', res['user']);
        setItem('role', res['role']);
        setItem('inventory', res['inventory']);
        return res;
      } else {
        localStorage.setItem('user', JSON.stringify(res['user']));
        return res;
      }
    }));
  }

  reLogin(data): Observable<any> {
    return this.http.post<any>(`${environment.server}/reLogin`, data).pipe(map(res => {
      // tslint:disable-next-line:triple-equals
      if (res['user'].pass_no_change == 0) {
        setToken(res['token']);
        setItem('user', res['user']);
        setItem('role', res['role']);
        setItem('inventory', res['inventory']);
        localStorage.removeItem('user');
        return res;
      } else {
        localStorage.setItem('user', JSON.stringify(res['user']));
        return res;
      }
    }));
  }

  logout(): Observable<any> {
    return this.http.post(`${environment.server}/logout`, {}).pipe(map(res => {
      sessionStorage.clear();
    }));
  }
  getToken() {
    return getToken();
  }
  getUser(): User  {
    return getItem('user') ? <User>getItem('user') : null;
  }

  getRol(): string {
    return getItem('role');
  }

  getInventory(): Inventory {
    return getItem('inventory');
  }
}
