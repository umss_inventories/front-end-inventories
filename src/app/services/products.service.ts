import {Injectable} from '@angular/core';
import {PrincipalService} from './principal.service';
import {Product} from '../model/products';
import {getItem} from './session';

@Injectable({
  providedIn: 'root'
})
export class ProductsService extends PrincipalService {
  getIndex(id?) {
    return this.http.get(`${this.server}/products/${id ? id : getItem('inventory').id}`);
  }

  create(data: Product) {
    data.id_inventory = getItem('inventory').id;
    return this.http.post(`${this.server}/products/create`, data);
  }
  update(data, id) {
    return this.http.post(`${this.server}/products/update/${id}`, data);
  }
  destroy(id: number) {
    return this.http.delete(`${this.server}/products/destroy/${id}`);
  }
}
