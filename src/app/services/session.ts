const roles = {
  1: 'Root',
  2: 'Administrador',
  3: 'Supervisor'
};

export function getUser() {
  return sessionStorage.getItem('user') ? JSON.parse(atob(sessionStorage.getItem('user'))) : null;
}

export function getRol(idRol) {
  return roles[idRol];
}

export function setItem(key, item) {
  sessionStorage.setItem(key, btoa(JSON.stringify(item)));
}

export function getItem(key) {
  return sessionStorage.getItem(key) ? JSON.parse(atob(sessionStorage.getItem(key))) : null;
}

export function getToken() {
  return sessionStorage.getItem('token');
}

export function setToken(token) {
  sessionStorage.setItem('token', token);
}

export function getInventory() {
  if (getUser().id_rol === 3) {
    return atob(sessionStorage.getItem('inventory'));
  } else {
    return getItem('inventory').name;
  }
}
