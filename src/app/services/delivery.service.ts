import { Injectable } from '@angular/core';
import {PrincipalService} from './principal.service';
import {Delivery} from '../model/delivery';
import {getItem} from './session';

@Injectable({
  providedIn: 'root'
})
export class DeliveryService extends PrincipalService {
  getIndex(id?) {
    return this.http.get(`${this.server}/delivery/${id ? id : getItem('inventory').id}`);
  }

  create(data: Delivery) {
    data.id_inventory = getItem('inventory').id;
    return this.http.post(`${this.server}/delivery/create`, data);
  }
  update(data, id) {
    return this.http.post(`${this.server}/delivery/update/${id}`, data);
  }
  destroy(id: number) {
    return this.http.delete(`${this.server}/delivery/destroy/${id}`);
  }
}
