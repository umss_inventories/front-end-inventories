import {Injectable} from '@angular/core';
import {PrincipalService} from './principal.service';

@Injectable({
  providedIn: 'root'
})
export class RolesService extends PrincipalService {
  getIndex() {
    return this.http.get(`${this.server}/roles`);
  }
}
