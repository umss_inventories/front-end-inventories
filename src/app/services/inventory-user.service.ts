import {Injectable} from '@angular/core';
import {PrincipalService} from './principal.service';

@Injectable({
  providedIn: 'root'
})
export class InventoryUserService extends PrincipalService {

  create(data) {
    return this.http.post(`${this.server}/userInventory/create`, data);
  }

  destroy(id: number) {
    return this.http.delete(`${this.server}/userInventory/destroy/${id}`);
  }
}
