import * as moment from 'moment';
import {MatPaginatorIntl} from '@angular/material';
import {User} from '../model/user';
import {getToken} from './session';


export function date(d) {
  return moment(d).format('YYYY-MM-DD');
}

export function existUser() {
  return getToken();
}

export function getUser(): User {
  return sessionStorage.getItem('user') ? <User>JSON.parse(atob(sessionStorage.getItem('user'))) : null;
}

const dutchRangeLabel = (page: number, pageSize: number, length: number) => {
  // tslint:disable-next-line:triple-equals
  if (length == 0 || pageSize == 0) {
    return `0 de ${length}`;
  }

  length = Math.max(length, 0);

  const startIndex = page * pageSize;

  // If the start index exceeds the list length, do not try and fix the end index to the end.
  const endIndex = startIndex < length ?
    Math.min(startIndex + pageSize, length) :
    startIndex + pageSize;

  return `${startIndex + 1} - ${endIndex} de ${length}`;
};

export function getDutchPaginatorIntl() {
  const paginatorIntl = new MatPaginatorIntl();

  paginatorIntl.itemsPerPageLabel = 'Mostrar:';
  paginatorIntl.nextPageLabel = 'Siguiente';
  paginatorIntl.previousPageLabel = 'Anterior';
  paginatorIntl.lastPageLabel = 'Ultimo';
  paginatorIntl.firstPageLabel = 'Primero';
  paginatorIntl.getRangeLabel = dutchRangeLabel;

  return paginatorIntl;
}
