import {Injectable} from '@angular/core';
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';
import * as moment from 'moment';
import {AuthService} from './auth.service';
import {getInventory} from './session';

@Injectable({
  providedIn: 'root'
})
export class PdfService {
  // faker = window.faker;
  constructor(private authService: AuthService) {
  }

  printPdf() {
  }

  createSingleTable(title, head, body, typeTable, optionCol) {
    const doc = new jsPDF();
    const base64Img = new Image();
    base64Img.src = '../../assets/umss.png';
    doc.addImage(base64Img, 'JPEG', 5, 5, 17, 25);
    doc.setFont('Arial');
    doc.textCenter('UNIVERSIDAD MAYOR DE SAN SIMÓN', {align: 'center'}, 0, 15);
    doc.textCenter('DIRECCIÓN DE PLANIFICACIÓN ACADÉMICA', {align: 'center'}, 0, 21);
    doc.setFontSize(14);
    doc.textCenter(getInventory().toUpperCase(), {align: 'center'}, 0, 27);
    const pageSize = doc.internal.pageSize;
    const pageWidth = pageSize.width ? pageSize.width : pageSize.getWidth();
    // doc.line(5, 18, 205, 18);
    // doc.addFont('ArialMS', 'Arial', 'normal');
    doc.setFont('Arial');
    doc.setFontSize(12);
    doc.textCenter(title, {align: 'center'}, 0, 34);
    const dataOptions = {
      startY: 41,
      head: [
        head
      ],
      body: body,
      margin: {
        top: 20,
        left: 15,
        right: 10,
        bottom: 10
      },
      theme: 'plain',
      tableLineColor: [222, 222, 222],
      styles: {
        cellPadding: [1.5, 1],
        lineColor: [191, 191, 191],
        lineWidth: 0.1,
        fontSize: 8
      },
      headStyles: {
        fillColor: [255, 255, 255],
        textColor: [75, 75, 75],
      },
      bodyStyles: {
        fillColor: [255, 255, 255],
        textColor: [75, 75, 75],
      },
      columnStyles: optionCol,
    };
    if (typeTable) {
      dataOptions['tableWidth'] = typeTable;
    }
    doc.autoTable(dataOptions);
    doc.save(`Report${moment().format('DD-MM-YYYY')}.pdf`);
  }
}

(function (API) {
  API.textCenter = function (txt, options, x, y) {
    options = options || {};
    if (options.align === 'center') {
      const fontSize = this.internal.getFontSize();
      const pageWidth = this.internal.pageSize.width;
      const txtWidth = this.getStringUnitWidth(txt) * fontSize / this.internal.scaleFactor;
      x = (pageWidth - txtWidth) / 2;
    }
    this.text(txt, x, y);
  };
})(jsPDF.API);
