import {Injectable} from '@angular/core';
import {PrincipalService} from './principal.service';
import {getItem} from './session';

@Injectable({
  providedIn: 'root'
})
export class UserService extends PrincipalService {
  getIndex() {
    return this.http.get(`${this.server}/user`);
  }

  getIndexInventory() {
    return this.http.get(`${this.server}/user/${getItem('inventory').id}`);
  }

  create(data) {
    return this.http.post(`${this.server}/user/create`, data);
  }

  update(data, id) {
    return this.http.post(`${this.server}/user/update/${id}`, data);
  }

  destroy(id: number) {
    return this.http.delete(`${this.server}/user/destroy/${id}`);
  }

  resetPass(data) {
    return this.http.post(`${this.server}/reset/pass`, data);
  }

  reset(data, id) {
    return this.http.put(`${this.server}/user/resetPass/${id}`, data);
  }
}
