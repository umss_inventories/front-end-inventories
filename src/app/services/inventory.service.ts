import {Injectable} from '@angular/core';
import {PrincipalService} from './principal.service';

@Injectable({
  providedIn: 'root'
})
export class InventoryService extends PrincipalService {
  getIndex() {
    return this.http.get(`${this.server}/inventory`);
  }

  create(data) {
    return this.http.post(`${this.server}/inventory/create`, data);
  }

  update(data, id) {
    return this.http.post(`${this.server}/inventory/update/${id}`, data);
  }

  destroy(id: number) {
    return this.http.delete(`${this.server}/inventory/destroy/${id}`);
  }

  getInventoryUsers(idInventory: number, idUser: number) {
    return this.http.get(`${this.server}/inventoryUser/${idInventory}/${idUser}`);
  }
}
