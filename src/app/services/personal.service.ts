import { Injectable } from '@angular/core';
import {PrincipalService} from './principal.service';
import {Personal} from '../model/personal';
import {getItem} from './session';

@Injectable({
  providedIn: 'root'
})
export class PersonalService extends PrincipalService {
  getIndex(id?) {
    return this.http.get(`${this.server}/personal/${id ? id : getItem('inventory').id}`);
  }

  create(data: Personal) {
    data.id_inventory = getItem('inventory').id;
    return this.http.post(`${this.server}/personal/create`, data);
  }
  update(data, id) {
    return this.http.post(`${this.server}/personal/update/${id}`, data);
  }
  destroy(id: number) {
    return this.http.delete(`${this.server}/personal/destroy/${id}`);
  }
}
