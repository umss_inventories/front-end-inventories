import { Injectable } from '@angular/core';
import {PrincipalService} from './principal.service';
import {Reception} from '../model/reception';
import {getItem} from './session';

@Injectable({
  providedIn: 'root'
})
export class ReceptionService extends PrincipalService {
  getIndex(id?) {
    return this.http.get(`${this.server}/reception/${id ? id : getItem('inventory').id}`);
  }

  create(data: Reception) {
    data.id_inventory = getItem('inventory').id;
    return this.http.post(`${this.server}/reception/create`, data);
  }
  update(data, id) {
    return this.http.post(`${this.server}/reception/update/${id}`, data);
  }
  destroy(id: number) {
    return this.http.delete(`${this.server}/reception/destroy/${id}`);
  }
}
