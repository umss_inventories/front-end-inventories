import { Injectable } from '@angular/core';
import {PrincipalService} from './principal.service';
import {Unity} from '../model/unity';
import {getItem} from './session';

@Injectable({
  providedIn: 'root'
})
export class UnityService extends PrincipalService {
  getUnities() {
    return this.http.get(`${this.server}/unity/${getItem('inventory').id}`);
  }

  create(data: Unity) {
    data.id_inventory = getItem('inventory').id;
    return this.http.post(`${this.server}/unity/create`, data);
  }

  update(data: Unity, id) {
    return this.http.post(`${this.server}/unity/update/${id}`, data);
  }

  destroy(id: number) {
    return this.http.delete(`${this.server}/unity/destroy/${id}`);
  }
}
