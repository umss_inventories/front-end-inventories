export class User {
  id: number;
  name: string;
  department: string;
  email: string;
  id_inventory?: number;
  pass_no_change?: number;
  id_rol: number;
}
