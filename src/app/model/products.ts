export class Product {
  id: number;
  name: string;
  id_unity: number;
  unity: string;
  quantity: number;
  id_inventory: number;
}
