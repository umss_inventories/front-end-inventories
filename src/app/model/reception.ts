export class Reception {
  id: number;
  id_product: number;
  product: string;
  id_user: number;
  name: string;
  quantity: number;
  date: number;
  id_inventory: number;
  unity?: any;
}
