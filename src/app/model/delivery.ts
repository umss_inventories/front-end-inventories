export class Delivery {
  id: number;
  id_product: number;
  product: string;
  id_user: number;
  id_personal: number;
  user_name: string;
  user_personal: string;
  quantity: number;
  date: number;
  id_inventory: number;
  unity?: string;
}
